"""
Database models for the file system application.

The `Inode` model represents a file or directory in the file system. It has the following fields:
- `id`: a unique identifier for the inode
- `name`: the name of the file or directory
- `is_file`: a boolean indicating whether the inode is a file or a directory
- `parent_id`: the ID of the parent inode, or `None` for the root directory
- `owner_id`: the ID of the user who owns the inode
- `creation_time`: the time the inode was created
- `path`: the full path of the inode in the file system

The `User` model represents a user of the file system application. It has the following fields:
- `id`: a unique identifier for the user
- `name`: the name of the user
- `password`: the user's password
"""

from . import db
from datetime import datetime

# Database Models...
class Inode(db.Model):
    __tablename__ = 'inodes'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(255), nullable=False)
    is_file = db.Column(db.Boolean, nullable=False)
    parent_id = db.Column(db.Integer, db.ForeignKey('inodes.id'), nullable=True)
    owner_id = db.Column(db.Integer, db.ForeignKey('users.id'), nullable=False)
    creation_time = db.Column(db.DateTime, default=datetime.now)
    path = db.Column(db.String(255), nullable=False)

    # cascade = True, delete relation...
    children = db.relationship("Inode", backref=db.backref('parent', remote_side=[id]), cascade="all, delete-orphan")

    def __repr__(self) -> str:
        return f"Inode('{self.name}', '{self.path}')"


class User(db.Model):
    __tablename__ = 'users'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(255), nullable=False)
    password = db.Column(db.String(255), nullable=False)

    def __repr__(self) -> str:
        return f"User('{self.id}', '{self.name}')"
