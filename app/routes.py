"""
The `views` blueprint contains the route definitions and corresponding view functions for the application.

The routes defined in this blueprint include:
- `home()`: Renders the home page, displaying the top-level files and directories.
- `all_files(inode_id)`: Renders a page displaying all files and directories within the specified directory.
- `upload()`: Handles file uploads, adding new files to the database and file system.
- `create()`: Handles directory creation, adding new directories to the database and file system.
- `download(inode_id)`: Allows downloading of files from the file system.
- `delete(inode_id)`: Deletes files or directories from the database and file system.
- `move(source_inode_id)`: Marks an inode for moving to a new location.
- `copy(source_inode_id)`: Marks an inode for copying to a new location.
- `paste(target_parent_id)`: Moves or copies the marked inode to the specified target directory.

The blueprint also includes two helper functions, `copy_children()` and `move_children()`, which recursively handle the copying or moving of child inodes when a directory is copied or moved.
"""


from . import db
from app.models import Inode
from app.utils import allowed_file, generate_path
from werkzeug.utils import secure_filename
from flask import request, render_template, send_file, url_for, flash, redirect
import os
import shutil
from flask import Blueprint


views = Blueprint('views', __name__)

tempInodeId = 0
move_in_progress = False

@views.route('/')
def home():
    inodes = Inode.query.filter_by(parent_id=1)
    parent_inode = Inode.query.filter_by(id=1).first()
    return render_template('home.html', inodes=inodes, parent_inode=parent_inode)


@views.route('/all_files/<int:inode_id>')
def all_files(inode_id):
    inodes = Inode.query.filter_by(parent_id = inode_id)
    parent_inode = Inode.query.filter_by(id=inode_id).first()
    return render_template('all_files.html', inodes=inodes, parent_inode=parent_inode)


@views.route('/upload', methods=['GET', 'POST'])
def upload():
    if request.method == 'POST':
        # Get the file from the request
        file = request.files['file']
        pid = int(request.form['parent_id'])

        # check is file already exists
        inode = Inode.query.filter_by(name=file.filename, parent_id=pid).first()
        if inode:
            flash(f"File already exists: {file.filename}")
            return redirect(url_for('views.all_files', inode_id=pid))

        # Check if file is not none and it is allowed (filetype)...
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)

            filepath = generate_path(filename, pid)
            file.save(filepath)

        new_inode = Inode(name=filename, is_file=True, parent_id=pid, owner_id=1, path=filepath)
        db.session.add(new_inode)
        db.session.commit()

    return redirect(url_for('views.all_files', inode_id=pid))


@views.route('/create', methods=['POST'])
def create():
    if request.method == 'POST':
        dirname = request.form['dirname']
        pid = int(request.form['parent_id'])

    # check if dir already exists? if not, create new inode and add it to the database
    try:
        dirpath=generate_path(dirname, pid)
        os.mkdir(dirpath)
        new_inode = Inode(name=dirname, is_file=False, parent_id=pid, owner_id=1, path=dirpath)
        db.session.add(new_inode)
        db.session.commit()

        flash(f'Directory {dirname} created successfully!')

    except Exception as e:
        flash(f"Error: {str(e)}")
        print(e)

    return redirect(url_for('views.all_files', inode_id=pid))



@views.route('/download/<int:inode_id>')
def download(inode_id):
    inode = Inode.query.filter_by(id=inode_id).first()
    if inode.is_file:
        file_to_send = os.path.abspath(inode.path)
        return send_file(file_to_send, as_attachment=True, download_name=inode.name)
    else:
        flash('Cannot download directory!')
        return redirect(url_for('views.all_files', inode_id=inode.parent_id))



@views.route('/delete/<int:inode_id>')
def delete(inode_id):
    inode = Inode.query.filter_by(id=inode_id).first()

    # if inode is file, delete the file
    if inode.is_file:
        os.remove(inode.path)
    else:
        # removes whole directory tree
        shutil.rmtree(inode.path)

    flash(f'{inode.name} deleted successfully!')
    db.session.delete(inode)
    db.session.commit()

    return redirect(url_for('views.all_files', inode_id=inode.parent_id))


@views.route('/move/<int:source_inode_id>')
def move(source_inode_id):
    global tempInodeId, move_in_progress
    move_in_progress = True
    tempInodeId = source_inode_id
    flash('Data cut successfully!')
    return redirect(url_for('views.home'))


@views.route('/copy/<int:source_inode_id>')
def copy(source_inode_id):
    global tempInodeId, move_in_progress
    move_in_progress = False
    tempInodeId = source_inode_id
    flash('Data copied successfully')
    return redirect(url_for('views.home'))


@views.route('/paste/<int:target_parent_id>')
def paste(target_parent_id):
    global tempInodeId, move_in_progress, children_nodes_list
    children_nodes_list = []  # Reset the children_nodes_list

    tempInode = Inode.query.filter_by(id=tempInodeId).first()

    # Logic for moving inodes...
    if move_in_progress:
        try:
            new_path = generate_path(tempInode.name, target_parent_id)
            if tempInode.is_file:
                if os.path.exists(new_path):
                    flash(f"File already exists: {new_path}")
                else:
                    shutil.move(tempInode.path, new_path)
                    tempInode.path = new_path
                    tempInode.parent_id = target_parent_id
                    flash(f'{tempInode.name} moved successfully!')

            else:
                if os.path.exists(new_path):
                    flash(f"Directory already exists: {new_path}")
                else:
                    shutil.move(tempInode.path, new_path)
                    tempInode.path = new_path
                    tempInode.parent_id = target_parent_id
                    move_children(tempInode.id)

                    flash(f'{tempInode.name} moved successfully!')
            db.session.commit()
            move_in_progress = False

        except Exception as e:
            flash(f"Error: {str(e)}")
            print(e)

    # Logic for copying inodes...
    else:
        new_name = f"copy__{tempInode.name}"
        new_path = generate_path(new_name, target_parent_id)

        try:
            if tempInode.is_file:
                if os.path.exists(new_path):
                    flash(f"File already exists: {new_path}")
                else:
                    shutil.copy(tempInode.path, new_path)
                    new_inode = Inode(
                        name=new_name,
                        is_file=tempInode.is_file,
                        parent_id=target_parent_id,
                        owner_id=tempInode.owner_id,
                        path=new_path
                    )
                    db.session.add(new_inode)
                    db.session.commit()
                    flash(f'{new_inode.name} pasted successfully!')

            else:
                if os.path.exists(new_path):
                    flash(f"Directory already exists: {new_path}")
                else:
                    shutil.copytree(tempInode.path, new_path)
                    new_inode = Inode(
                        name=new_name,
                        is_file=tempInode.is_file,
                        parent_id=target_parent_id,
                        owner_id=tempInode.owner_id,
                        path=new_path
                    )
                    db.session.add(new_inode)
                    db.session.commit()
                    copy_children(tempInode.id, new_inode.id)
                    flash(f'{new_inode.name} pasted successfully!')

        except Exception as e:
            flash(f"Error: {str(e)}")
            print(e)

    return redirect(url_for('views.home'))



# Recurssive copy children function which gets all the children nodes and creates a copy of it.
def copy_children(source_parent_id, new_parent_id):
    Children_Inodes = Inode.query.filter_by(parent_id=source_parent_id).all()
    for child in Children_Inodes:

        new_inode = Inode(
            name=child.name,
            is_file=child.is_file,
            parent_id=new_parent_id,
            owner_id=child.owner_id,
            path=generate_path(child.name, new_parent_id)
        )
        db.session.add(new_inode)
        db.session.commit()
        if not child.is_file:
            copy_children(child.id, new_inode.id)


# Recurssive move children function which gets all the children and updates their path and parent id.

def move_children(parent_inode_id):
    Children_Inodes = Inode.query.filter_by(parent_id=parent_inode_id).all()
    for child in Children_Inodes:
        child.pid = parent_inode_id
        child.path = generate_path(child.name, parent_inode_id)
        if not child.is_file:
            move_children(child.id)
        db.session.commit()

