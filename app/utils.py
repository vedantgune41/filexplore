"""
Utility functions for handling file uploads and generating file paths.

The `allowed_file` function checks if a given filename has an allowed file extension.
The `generate_path` function generates a file path for a given filename and parent directory.
"""


from app.models import Inode, User
from . import db
import os

def allowed_file(filename):
    ALLOWED_EXTENSIONS = {'txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif', 'mp4', 'docx', 'pptx'}
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


def generate_path(filename, parent_id=1):
    parent_node = Inode.query.filter_by(id=parent_id).first()
    path = os.path.join(parent_node.path,  filename)

    return path.replace('\\', '/')

def populate_database():
    # Check if the database is empty
    if not User.query.first():
        # Add a default user
        user = User(name='admin', password='admin')
        db.session.add(user)
        db.session.commit()

    if not os.path.exists('./uploads/root'):
        os.mkdir('./uploads/root')

    if not Inode.query.first():
        # Add a root inode
        root_inode = Inode(name='root', is_file=False, parent_id=None, owner_id=user.id, path='./uploads/root')
        db.session.add(root_inode)
        db.session.commit()
