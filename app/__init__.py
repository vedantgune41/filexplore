"""
Creates a Flask application instance and initializes the SQLAlchemy database.

The `create_app` function is a factory function that creates and configures a Flask application instance. It sets up the SQLAlchemy database connection and registers the blueprint for the application's routes.

Args:
    configurations (optional): A configuration object that can be used to override the default configuration.

Returns:
    A configured Flask application instance.
"""
from flask import Flask
from flask_sqlalchemy import SQLAlchemy


db = SQLAlchemy()

def create_app(configurations = None):
    app = Flask(__name__, instance_relative_config=True)

    if configurations:
        app.config.from_object(configurations)
    
    else:
        app.config.from_object('config.Config')
    

    db.init_app(app)

    from .routes import views
    app.register_blueprint(views)

    return app
