"""
This script creates and runs the Flask application for the app.

The `create_app()` function is imported from the `app` module and used to create the Flask application instance. The application is then run in debug mode when this script is executed directly (i.e. not imported as a module).
"""

from app import create_app, db
from app.utils import populate_database
from instance.config import ProductionConfig

app = create_app(ProductionConfig)


if __name__ == "__main__":
    app = create_app()
    with app.app_context():
        db.create_all()  # Ensure all tables are created
        populate_database()  # Populate the database if empty
    app.run(debug=True, host="0.0.0.0", port=5000)
