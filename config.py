"""
This module defines the configuration settings for the application.

The `Config` class contains the following configuration settings:

- `SECRET_KEY`: The secret key used for session management and other security-related tasks. If not provided in the environment, it defaults to 'FileXplorer@123'.
- `SQLALCHEMY_DATABASE_URI`: The database connection URI used by SQLAlchemy. If not provided in the environment, it defaults to 'mysql://root:root@localhost/explorerDB'.
- `SQLALCHEMY_TRACK_MODIFICATIONS`: A flag indicating whether SQLAlchemy should track modifications to objects. This is set to `False` to improve performance.
- `UPLOAD_FOLDER`: The directory where uploaded files are stored. If not provided in the environment, it defaults to 'uploads'.
- `TESTING`: A flag indicating whether the application is running in a testing environment. This is set to `False` by default.
"""

import os


class Config:
    SECRET_KEY = os.getenv("SECRET_KEY", "File_Xplorer_1234")
    SQLALCHEMY_DATABASE_URI = "mysql://root:root@database/explorerDB"
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    UPLOAD_FOLDER = os.getenv("UPLOAD_FOLDER", "uploads")
    TESTING = False
