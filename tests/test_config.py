"""
Configuration settings for the FileXplore unit tests.

The `TestConfig` class defines the configuration settings used for the FileXplore
unit tests. These settings include:

- `SECRET_KEY`: A secret key used for cryptographic functions.

- `SQLALCHEMY_DATABASE_URI`: The SQLAlchemy database URI, which is set to an in-memory
  SQLite database for testing purposes.

- `SQLALCHEMY_TRACK_MODIFICATIONS`: Disables SQLAlchemy's tracking of object
  modifications, which is not needed for testing.

- `UPLOAD_FOLDER`: The temporary directory used for file uploads during testing.

- `TESTING`: Enables the testing mode, which changes certain behaviors for the
  application during testing.

"""

import tempfile

class TestConfig:
    SECRET_KEY = 'FileXplore_UnitTesting@0.2'
    SQLALCHEMY_DATABASE_URI = 'sqlite:///:memory:'
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    UPLOAD_FOLDER = tempfile.mkdtemp()
    TESTING = True
