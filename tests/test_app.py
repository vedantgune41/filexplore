"""
This test suite contains various test cases for the application's functionality.

The `TestApplication` class inherits from `unittest.TestCase` and provides methods to set up and tear down the test environment, as well as several test cases to verify the behavior of the application's features.

The test cases cover the following functionality:
- Home page
- Listing all files
- Uploading a file
- Creating a directory
- Downloading a file
- Deleting a file
- Moving a file
- Copying a file
- Moving and pasting a file
- Copying and pasting a file

Each test case sets up the necessary data and environment, executes the corresponding action, and verifies the expected outcome.
"""
# test_app.py

from pathlib import Path
from tests.test_config import TestConfig
from app import create_app, db
from app.utils import generate_path
from app.models import User, Inode
from io import BytesIO
from werkzeug.utils import secure_filename
import unittest
import os
import shutil

def create_test_inode(file_path):
    if isinstance(file_path, str):
        file_path = Path(file_path)
    return Inode(name=file_path.name, is_file=True, parent_id=1, owner_id=1, path=file_path.__str__())

class TestApplication(unittest.TestCase):

    def initialize_test_environment(self) -> None:
        # Get upload folder and its path
        upload_folder = self.app.config['UPLOAD_FOLDER']
        upload_folder_path = Path(os.path.join(upload_folder, 'testroot'))
        self.tempdir = upload_folder

        # Create upload folder if it does not exist.
        os.makedirs(upload_folder_path, exist_ok=True)

        # Create dummy user and inode for testing
        user = User(id=1, name='test_user', password='test_password')
        db.session.add(user)

        inode = create_test_inode(upload_folder_path)
        db.session.add(inode)

        # Save changes to db session
        db.session.commit()



    def setUp(self) -> None:
        # Create instance of app & load configurations from TestConfig
        self.app = create_app(TestConfig)
        self.app_context = self.app.app_context()
        self.app_context.push()
        self.client = self.app.test_client()


        # Create all tables in the database
        db.create_all()
        self.initialize_test_environment()



    def tearDown(self) -> None:
        db.session.remove()
        if os.path.exists(self.tempdir):
            shutil.rmtree(self.tempdir)
        db.drop_all()
        self.app_context.pop()



    def test_home(self) -> None:
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)



    def test_allfiles(self) -> None:
        response = self.client.get('/all_files/1')
        self.assertEqual(response.status_code, 200)



    def test_upload(self):
        filename = 'test 1 file.txt'
        pid = 1

        content = BytesIO(b'Hello, Tests!\nTest File Data')
        filedata = {
            'file' :  (content, filename),
            'parent_id' : pid
        }

        response = self.client.post('/upload', data=filedata, content_type='multipart/form-data')

        self.assertEqual(response.status_code, 302)

        # check if file exists logically
        filename_corrected = secure_filename(filename)
        file_inode = Inode.query.filter_by(name=filename_corrected, parent_id=pid).first()
        self.assertEqual(filename_corrected, file_inode.name)


        # check if file exists physically
        uploaded_file = Inode.query.filter_by(name=filename_corrected, parent_id=pid).first()
        self.assertTrue(os.path.exists(uploaded_file.path))



    def test_create(self):
        dirname = 'test_dir'
        pid = 1

        # proxy dir created
        dirdata = {
            'dirname' : dirname,
            'parent_id': pid
        }

        response = self.client.post('/create', data=dirdata)
        self.assertEqual(response.status_code, 302)

        # Check if dir exists locally
        dir_inode = Inode.query.filter_by(name=dirname, parent_id=pid).first()
        self.assertEqual(dirname, dir_inode.name)

        # Check if dir exists physically
        uploaded_dir = Inode.query.filter_by(name=dirname, parent_id=pid).first()
        self.assertTrue(os.path.exists(uploaded_dir.path))

    def test_download(self):
        file_path = Path(generate_path('test_file.txt', 1))

        with open(file_path, 'w') as f:
            f.write('file contents')

        inode = create_test_inode(file_path)
        db.session.add(inode)
        db.session.commit()

        response = self.client.get(f'/download/{inode.id}')
        response.close()
        self.assertEqual(response.status_code, 200)

    def test_delete(self):
        file_path = Path(os.path.join(self.tempdir, 'file.txt'))

        with open(file_path, 'w') as f:
            f.write('file contents')

        inode = create_test_inode(file_path)
        db.session.add(inode)
        db.session.commit()

        response = self.client.get(f'/delete/{inode.id}')
        self.assertEqual(response.status_code, 302)

        self.assertIsNone(Inode.query.filter_by(id=inode.id).first())
        self.assertFalse(os.path.exists(file_path))



    def test_move(self):
        file_path = os.path.join(self.tempdir, 'file.txt')
        with open(file_path, 'w') as f:
            f.write('file contents')
        inode = create_test_inode(file_path)
        db.session.add(inode)
        db.session.commit()
        response = self.client.get(f'/move/{inode.id}')
        self.assertEqual(response.status_code, 302)



    def test_copy_file(self):
        file_path = os.path.join(self.tempdir, 'file.txt')
        with open(file_path, 'w') as f:
            f.write('file contents')
        inode = create_test_inode(file_path)
        db.session.add(inode)
        db.session.commit()
        response = self.client.get(f'/copy/{inode.id}')
        self.assertEqual(response.status_code, 302)


    def test_move_paste(self):
        source_file_path = os.path.join(self.tempdir, 'file.txt')
        with open(source_file_path, 'w') as f:
            f.write('file contents')
        source_inode = create_test_inode(source_file_path)
        db.session.add(source_inode)
        db.session.commit()

        response = self.client.get(f'/move/{source_inode.id}')
        self.assertEqual(response.status_code, 302)

        target_dir_path = os.path.join(self.tempdir, 'new_dir')
        os.mkdir(target_dir_path)
        target_parent = create_test_inode(target_dir_path)
        db.session.add(target_parent)
        db.session.commit()

        response = self.client.get(f'/paste/{target_parent.id}')
        self.assertEqual(response.status_code, 302)

        childrenInodes = Inode.query.filter_by(parent_id=target_parent.id).all()
        for child in childrenInodes:
            if child.name == 'file.txt':
                self.assertEqual('file.txt', child.name)

        moved_file_path = os.path.join(target_dir_path, 'file.txt')
        self.assertTrue(os.path.exists(moved_file_path))
        self.assertFalse(os.path.exists(source_file_path))


    def test_copy_paste(self):
        source_file_path = generate_path('file.txt', 1)
        with open(source_file_path, 'w') as f:
            f.write('file contents')

        source_inode = create_test_inode(source_file_path)
        db.session.add(source_inode)
        db.session.commit()

        self.client.get(f'/copy{source_inode.id}')

        target_dir_path = generate_path('new_dir', 1)
        os.mkdir(target_dir_path)
        target_parent = create_test_inode(target_dir_path)
        db.session.add(target_parent)
        db.session.commit()

        self.assertTrue(os.path.exists(target_dir_path))
        self.assertTrue(os.path.exists(source_file_path))

        response = self.client.get(f'/paste/{target_parent.id}')
        self.assertEqual(response.status_code, 302)

        children = Inode.query.filter_by(parent_id = target_parent.id).all()
        for child in children:
            self.assertTrue(os.path.exists(child.path))

        children = Inode.query.filter_by(parent_id = 1).all()
        for child in children:
            if child.name == 'file.txt':
                self.assertTrue(os.path.exists(child.path))



        self.assertTrue(os.path.exists(child.path))

if __name__ == '__main__':
    unittest.main()
