

# FileXplore

FileXplore is a file explorer web application for managing files and directories, allowing users to upload, download, delete, move, and copy files and directories. It is built using Flask and (MySQL)SQLAlchemy.

## Features

- Upload files
- Create directories
- Download files
- Delete files and directories
- Move and copy files and directories

## Prerequisites

- Python 3.6 or higher
- MySQL database

## Installation

1. **Clone the repository:**

    <strong>bash</strong>
    ```
    git clone https://github.com/yourusername/file-explorer.git
    cd file-explorer
    ```

2. **Create and activate a virtual environment:**

    <strong>bash</strong>
    ```
    python -m venv venv
    venv\Scripts\activate   # On Windows
    # or
    source venv/bin/activate  # On Unix or MacOS
    ```

3. **Install the required dependencies:**

    <strong>bash</strong>
    ```
    pip install -r requirements.txt
    ```

4. **Configure the database:**

    - Open `config.py` and update the database URI with your MySQL credentials.

    <strong>python</strong>
    ```
    app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://username:password@localhost/explorerDB'
    ```

5. **Initialize the database:**

    <strong>python</strong>
    ```
    from config import app, db

    with app.app_context():
        db.create_all()
    ```

6. **Run the application:**

    <strong>bash</strong>
    ```
    flask run
    # OR
    python app.py
    ```

    The application will be available at `http://127.0.0.1:5000`.

## Project Structure

- `app.py` - The main Flask application file.
- `config.py` - Configuration file for Flask and SQLAlchemy.
- `utils.py` - Utility functions for the application.
- `templates/` - HTML templates for rendering web pages.
- `uploads/` - Directory for storing uploaded files.
- `venv/` - Virtual environment directory (ignored by git).
- `__pycache__/` - Python bytecode cache directory (ignored by git).

## Gitignore

Ensure that certain directories and files are ignored by git by including them in the `.gitignore` file:

<strong>.gitignore</strong>
```
uploads/
__pycache__/
venv/
```

## Usage

### Upload a File

1. Navigate to the upload page.
2. Choose a file to upload and select the parent directory.
3. Click on the "Upload" button.

### Create a Directory

1. Navigate to the create directory page.
2. Enter the directory name and select the parent directory.
3. Click on the "Create" button.

### Download a File

1. Navigate to the file you want to download.
2. Click on the download link.

### Delete a File or Directory

1. Navigate to the file or directory you want to delete.
2. Click on the delete link.

### Move or Copy a File or Directory

1. Navigate to the file or directory you want to move or copy.
2. Click on the move or copy link.
3. Navigate to the target directory and click on the paste link.

## License

This project is licensed under the MIT License. See the [LICENSE](LICENSE) file for details.

## Contributing

Contributions are welcome! Please create a pull request or open an issue to discuss your changes.

## Acknowledgements

- Flask
- MySQL
- SQLAlchemy
- Werkzeug