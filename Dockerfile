FROM python:3.12-slim



WORKDIR /application

COPY . .

RUN apt-get update && apt-get install -y \
    default-libmysqlclient-dev \
    build-essential \
    pkg-config 

RUN pip install --no-cache-dir -r requirements.txt
